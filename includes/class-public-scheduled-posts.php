<?php

class Public_Scheduled_Posts {
	private $saved_results;

	public function run() {
		add_filter( 'posts_results', [ &$this, 'save_results' ], null, 2 );
	}

	public function save_results( $posts, &$query ) {
		if ( ! empty( $posts )
			&& $query->is_single
			&& 'future' === get_post_status( $posts[0] ) ) {
			$this->saved_results = $posts;
			add_filter( 'the_posts', [ &$this, 'restore_results' ], null, 2 );
		}

		return $posts;
	}

	public function restore_results( $posts, &$query ) {
		remove_filter( 'the_posts', [ &$this, 'restore_results' ], null, 2 );
		return $this->saved_results;
	}
}
