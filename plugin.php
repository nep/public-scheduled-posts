<?php

/*
Plugin Name: Public Scheduled Posts
Plugin URI: https://gitlab.com/nep/public-scheduled-posts
Description: Permits public access to scheduled posts.
Version: 0.1.0
Author: Andrew <andrew@nationaleventpros.com>
Author URI: https://encrypt.to/0x677142D28B141165
License: GPL-3.0
License URI: https://www.gnu.org/licenses/gpl-3.0.html

This plugin is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This plugin is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this plugin. If not, see <http://www.gnu.org/licenses/>.
*/

if ( ! defined( 'WPINC' ) ) {
	die;
}

require plugin_dir_path( __FILE__ ) . 'includes/class-public-scheduled-posts.php';

$plugin = new Public_Scheduled_Posts();
$plugin->run();
