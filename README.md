# Public Scheduled Posts

A WordPress plugin to permit public access to scheduled posts

## Installation

This plugin has not been published to the WordPress Plugins Directory so it must
be installed ["manually"][Manual_Plugin_Installation]:

```shell
git clone 'https://gitlab.com/nep/public-scheduled-posts.git' \
          'wp-content/plugins/public-scheduled-posts'
wp plugin activate 'public-scheduled-posts'
```

## Usage

To view a scheduled post, visit its permalink.

## License

This plugin is licensed under the [GNU General Public License v3.0].


[GNU General Public License v3.0]: https://www.gnu.org/licenses/gpl-3.0.html
[Manual_Plugin_Installation]: https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation
